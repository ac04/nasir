// (function() {

	var basket = [];
	var quantity = 1;
	var isChangingQuantity = false;
	var subtotal = 0.00;
	var vat = 0.00;
	var total = 0.00;
	var paid = 0.00;
	var change = 0.00;
	var paymentType = 'cash';


	$('#barcode').focus();

	$('.keypad .button.number-button').click(function() {
		
		if (""+$(this).data('num-val') !== "undefined") {
		
			$('#barcode').val($('#barcode').val() + "" + $(this).data('num-val')).focus();

			return false;

		}

	});

	$('.keypad .button.delete-button').click(function() {
		
		var val = $('#barcode').val();
		$('#barcode').val(val.substring(0, val.length-1));
		return false;

	});

	$('.keypad .button.quantity-button').click(function() {

		isChangingQuantity = true;
		$('#barcode').attr('placeholder', 'Enter Quantity').focus();
		$('.barcode-quantity-label').text('Quantity: ');


		return false;

	});

	$('.barcode-quantity').on('submit', function(event) {
		
		if (isChangingQuantity) {

			quantity = Number($('#barcode').val());
			isChangingQuantity = false;
			$('#barcode').attr('placeholder', 'Enter a barcode');
			$('.barcode-quantity-label').text('Barcode: ');

		} else {

			handleItemAddToBasket();
			
		}

		$('#barcode').val('');

		return false;

	});


	$('.keypad .button.enter-button').click(function() {

		if (isChangingQuantity) {

			quantity = Number($('#barcode').val());
			isChangingQuantity = false;
			$('#barcode').attr('placeholder', 'Enter a barcode');

		} else {

			handleItemAddToBasket();

		}

		$('#barcode').val('').focus();

		return false;

	});

	$('.void-item-button').click(function() {

		if($('.transactions-list tr.selected').length > 0) {
			
			var barcode = $('.transactions-list tr.selected').data('barcode');

			var basketItem = getBasketItem(barcode, true);

			basket = _.without(basket, basketItem.item);

			renderBasket();

		}

		return false;

	});

	$('.void-basket-button').click(function() {

		basket = [];
		renderBasket();

		return false;

	});



	$('.keypad .button.checkout-button').click(function() {
		
		if (basket.length > 0) {

			var priceButtons = (
				`<div class="columns is-multiline">
					<div class="column is-12">
						<div class="columns is-multiline">
							<div class="column is-3">
								<a href="" class="button is-large is-info is-outlined exact-cash-button">£${total.toFixed(2)}</a>
							</div>
							<div class="column is-3">
								<a href="" class="button is-large is-info is-outlined cash-button-5">£5</a>
							</div>
							<div class="column is-3">
								<a href="" class="button is-large is-info is-outlined cash-button-10">£10</a>
							</div>
							<div class="column is-3">
								<a href="" class="button is-large is-info is-outlined cash-button-20">£20</a>
							</div>
							<div class="column is-3">
								<a href="" class="button is-large is-info is-outlined cash-button-50">£50</a>
							</div>
							<div class="column is-3">
								<a href="" class="button is-large is-info is-outlined cash-button">Cash</a>
							</div>
							<div class="column is-3">
								<a href="" class="button is-large is-info is-outlined card-button">Card</a>
							</div>
						</div>
					</div>
					
				</div>
			`);

			$('.top-keys').html(priceButtons);
			$('.bottom-keys a').attr('disabled', 'disabled');
			if ($('.subtotal').length > 0) {

			} else {
				$('.transactions-list .transactions').append((
					`<tr data-barcode="${item.barcode}">
						<td></td>
						<td>Subtotal</td>
						<td></td>
						<td>£${total.toFixed(2)}</td>
					</tr>`));
			}

		}
		
		return false;

	});

	$('body').on('click', '.cash-button-5', function() {

		handlePayment(5.00);
		return false;

	});

	$('body').on('click', '.cash-button-10', function() {

		handlePayment(10.00);
		return false;

	});

	$('body').on('click', '.cash-button-20', function() {

		handlePayment(20.00);
		return false;

	});

	$('body').on('click', '.cash-button-50', function() {

		handlePayment(50.00);
		return false;

	});

	$('body').on('click', '.transactions-list tr', function() {
		if (!$(this).hasClass('unselectable')) {
			
			if ($(this).hasClass('selected')) {
				
				$('.transactions-list tr').removeClass('selected');

			} else {
				
				$('.transactions-list tr').removeClass('selected');
				$(this).addClass('selected');	


			}

		}
	});

	$('.store-select .button').on('click', function() {
		$('.store-select').toggleClass('is-active');
	});

	$( "a[disabled]" ).on('click', function() {
		return false;
	});

	function renderBasket() {

		total = 0, vat = 0, subtotal = 0;

		$('.transactions-list table.transactions').html((
			`<tr class='unselectable'>
				<th>Qty/kg</th>
				<th>Description</th>
				<th>Price</th>
				<th>Total</th>
			</tr>`
		));

		if (basket.length > 0) {
			$('.checkout-button').attr('disabled', false);
		} else {
			$('.checkout-button').attr('disabled', 'disabled');
		}

		$.each(basket, function(index, item) {
			if (item !== undefined) {
				var cost = item.price * item.quantity;
				subtotal += cost - (item.quantity * item.vat);
				console.log(subtotal);
				vat += item.quantity * item.vat;
				total += cost;
				$('.transactions-list table.transactions').append((
					`<tr data-barcode="${item.barcode}">
						<td>${item.quantity}</td>
						<td>${item.description}</td>
						<td>£${item.price}</td>
						<td>£${cost.toFixed(2)}</td>
					</tr>`
				));
			}
			

		});

		$('#total').text("£" + total.toFixed(2));

	}

	function getBasketItem(barcode, includeIndex) {
		
		for(var a = 0; a < basket.length; a++) {
		
			var item = basket[a];
			
			if (item !== undefined) {
				
				if (item.barcode == barcode) {
			
					if (includeIndex) {
						
						return {
							item: item, 
							index: a
						};

					} else {
						
						return item;

					}
			
				}

			}
			
		}
		
		return false;

	}

	function handleItemAddToBasket() {
		
		if ($('#barcode').val().length > 0) {
			
			var barcode = $('#barcode').val();
			$.get('/api/product', {barcode: $('#barcode').val()}, function(response) {
				
				var response = JSON.parse(response);
				
				if (response.status == "error") {
					return false;
				}

				var product = response.product;

				if (item = getBasketItem(barcode, false)) {

					item.quantity += quantity;

				} else {
					
					basket.push({
						quantity: quantity,
						description: product.description,
						price: Number(product.retail_price),
						barcode: Number(product.barcode),
						vat: Number(product.vat)
					});

				}
				
				quantity = 1;
				renderBasket();

			});

		}
	}

	function handlePayment(value) {
		
		paid += value;
		remaining = total - paid;
		if (remaining < 0) {
			change = Math.abs(total - paid);
			remaining = 0;
		}



		if($('.paid-amount').length < 1) {

			$('.transactions-list .transactions tbody').append(`
				<tr class='paid-amount'>
					<td></td>
					<td>Paid</td>
					<td></td>
					<td class='value'>£${paid.toFixed(2)}</td>
				</tr>
				<tr class='remaining-amount ${(remaining > 0) ? "" : "hidden"}'>
					<td></td>
					<td>Remaining</td>
					<td></td>
					<td class='value'>£${remaining.toFixed(2)}</td>
				</tr>
				<tr class='change-amount ${(change > 0) ? "" : "hidden"}'>
					<td></td>
					<td>Change</td>
					<td></td>
					<td class='value'>£${change.toFixed(2)}</td>
				</tr>
			`);
			
		}

		$('.paid-amount .value').text(`£${paid.toFixed(2)}`);
		$('.remaining-amount .value').text(`£${remaining.toFixed(2)}`);
		$('.change-amount .value').text(`£${change.toFixed(2)}`);



		if (remaining == 0) {

			$('.remaining-amount').addClass('hidden');
			$('.change-amount').removeClass('hidden');

			// record transaction
			$.post('/api/transaction', {
				branch_id: $('#branch_id').val(), 
				user_id: $('#user_id').val(),
				subtotal: subtotal,
				vat: vat,
				total: total,
				payment_type: paymentType,
				type: 'sale',
				basket: basket,
				_token: $('#csrf_token').val()
			}, function(response) {

				response = JSON.parse(response);

				console.log(response);

				if (response.status == "success") {

					window.setTimeout(function() {

						startNewTransaction();

					}, 3000);

				}

			});

		}





		

	}

/*


if (paid < total) {



			if ($('.paid-amount').length > 0) {

				$('.paid-amount').html((
					`<td></td>
					<td>Paid</td>
					<td></td>
					<td>£${paid.toFixed(2)}</td>
				`));
				if (paid < total) {
					var remaining = total - paid;
					$('.remaining-amount').html((
						`<td></td>
						<td>Remaining</td>
						<td></td>
						<td>£${remaining.toFixed(2)}</td>
					`));

				} else {

					$('.remaining-amount').remove();


					if ($('.transactions-list .transactions .change-amount').length == 0) {

						var change = paid - total;
						$('.transactions-list .transactions').append((
							`<tr class='change-amount'>
								<td></td>
								<td>Change</td>
								<td></td>
								<td>£${change.toFixed(2)}</td>
							</tr>
						`));
					}
					

				}

			} else {

				$('.transactions-list .transactions').append((
					`<tr class='paid-amount'>
						<td></td>
						<td>Paid</td>
						<td></td>
						<td>£${paid.toFixed(2)}</td>
					</tr>
				`));
				if (paid < total) {

					var remaining = total - paid;
					$('.transactions-list .transactions').append((
						`<tr class='remaining-amount'>
							<td></td>
							<td>Remaining</td>
							<td></td>
							<td>£${remaining.toFixed(2)}</td>
						</tr>
					`));

				} else {

					if ($('.transactions-list .transactions .change-amount').length == 0) {

						var change = paid - total;
						$('.transactions-list .transactions').append((
							`<tr class='change-amount'>
								<td></td>
								<td>Change</td>
								<td></td>
								<td>£${change.toFixed(2)}</td>
							</tr>
						`));
					}

				}
			
			}

		} else {

			// record transaction
			$.post('/api/transaction', {
				branch_id: $('#branch_id').val(), 
				user_id: $('#user_id').val(),
				subtotal: subtotal,
				vat: vat,
				total: total,
				payment_type: paymentType,
				type: 'sale',
				_token: $('#csrf_token').val()
			}, function(response) {

				response = JSON.parse(response);

				if (response.status == "success") {

					window.setTimeout(function() {

						startNewTransaction();

					}, 3000);

				}

			});

		}


*/



	function startNewTransaction() {

		basket = [];
		quantity = 1;
		isChangingQuantity = false;
		subtotal = 0.00;
		vat = 0.00;
		total = 0.00;
		paid = 0.00;
		change = 0.00;

		$('.transactions-list .transactions').html((
			`<tr class='unselectable'>
				<th>Qty/kg</th>
				<th>Description</th>
				<th>Price</th>
				<th>Total</th>
			</tr>
		`));

		$('.top-keys').html(`
			<div class="columns is-multiline">
				<div class="column is-9">
					<div class="columns is-multiline">
						<div class="column is-4">
							<a href="" class="button is-large is-info is-outlined quantity-button">Qty</a>
						</div>
						<div class="column is-4">
							
						</div>
						<div class="column is-4">
							
						</div>
					</div>
				</div>
				
			</div>
		`);

		$('.bottom-keys a').attr('disabled', false);

		$('#total').text("£" + total.toFixed(2));


	}

// })();