<?php

	namespace App\Http\Responses;
	
	
	class ProductStockQuantityJSONResponse {

		var $status = "";
		var $product = null;
		var $stockQuantities = [];

	}