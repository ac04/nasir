<?php

	namespace App\Http\Controllers;
	use Illuminate\Http\Request;
	use App\Http\Models\Product;
	use App\Http\Models\Transaction;
	use App\Http\Models\Branch;


	class ImportController extends Controller {

		private $importStarted = false;

		public function import() {

			$this->printMessage("Starting import of products...", 0);


			//open CSV file

			$file = file('../sample products.csv');

			// loop over CSV file line by line

			foreach($file as $line) {

				// split line by comma

				list($barcode, $description, $department, $vat, $costPrice, $retailPrice, $margin) = explode(',', $line);

				$this->printMessage("Importing product\t" . $barcode . "\t" . $description, 1);
				
				// create a new Product and populate  
				$product = new Product();

				$product->barcode = (int) $barcode;
				$product->description = $description;
				$product->department = $department;
				$product->vat = $vat;
				$product->cost_price = $costPrice;
				$product->markup = $margin;
				$product->retail_price = $retailPrice;
				$product->save();  // save the product to the database

				// generate random stock quantity for branch 1 (George's Convenience Store)
				$branch1 = Branch::find(1);
				$quantity = rand(0,20);
				$branch1->products()->attach($product, ['quantity' => $quantity]);
				$this->printMessage("Generating stock value for " . $branch1->name . "\t" . $quantity, 2);
				
				// generate random stock quantity for branch 2 (A & Z Convenience Store)
				$branch2 = Branch::find(2);
				$quantity = rand(0,20);
				$branch2->products()->attach($product, ['quantity' => $quantity]);
				$this->printMessage("Generating stock value for " . $branch2->name . "\t" . $quantity, 2);

			}

		}

		public function createBranches() {

			$branch1 = new Branch();
			$branch1->name = "George's Convenience Store";
			$branch1->address_line_1 = "162 New Cheltenham Road";
			$branch1->town = "Kingswood";
			$branch1->city = "Bristol";
			$branch1->postcode = "BS15 1UN";
			$branch1->save();

			$branch2 = new Branch();
			$branch2->name = "Store 2";
			$branch2->address_line_1 = "Store 2 line 1";
			$branch2->town = "Kingswood";
			$branch2->city = "Bristol";
			$branch2->save();

		}

		private function printMessage($message, $level) {

			if (!$this->importStarted) {
				echo "<pre>";
				$this->importStarted = true;
			}

			for($a = 0; $a < $level; $a++) {

				echo "\t";

			}

			echo $message . "\n";
		}

	}