@extends('pos.layout')
@section('bodyClass', 'pos-receive')

@section('nav')

<nav class="navbar main-nav" role="navigation" aria-label="main navigation">
	<div class="navbar-brand">
		<a class="navbar-item" href="/">
			<img src="http://placehold.it/100x100" alt="Bulma: a modern CSS framework based on Flexbox" > Home
		</a>
	</div>
	<div class="navbar-start">
		<a class="navbar-item" href='/stock/sell'>Sell Stock</a>
		<a class="navbar-item" href='/stock/receive'>Receive Stock</a>
		@if (Auth::user()->is_admin)
		<a class="navbar-item" href='/admin'>Admin</a>
		@endif 
	</div>
	<div class="navbar-end">
		<a class="navbar-item" href="/auth/logout">Logout</a>
	</div>
		<button class="button navbar-burger">
			<span></span>
			<span></span>
			<span></span>
		</button>
	</div>	
</nav>

@endsection

@section('content')

<h1>Receive Stock For {{$store->name}}</h1>
<section class="section">
	<!-- <div class="columns"> -->
<!-- Main container -->

	<nav class="level">
		<!-- Left side -->
		<div class="level-left">
			<div class="level-item">
				
				<form action="" id="receive-stock-form" method="post">
					<div class="field is-horizontal">
						<div class="field-label is-normal">
							<label class="label has-text-right">Barcode</label>
						</div>
						<div class="field-body">
							<div class="field">
								<p class="control is-expanded">
									<input class="input" type="text" id="barcode" placeholder="Barcode">
								</p>
							</div>
						</div>
						<div class="field-label quantity-label is-normal">
							<label class="label has-text-right">Quantity</label>
						</div>
						<div class="field-body">

							<div class="field">
								<p class="control is-expanded">
									<input class="input" id="quantity" type="text" placeholder="1" >
								</p>
							</div>
						</div>

						
						
						<div class="field">
							<p class="control is-expanded">
								<input type="submit" class='button receive-submit-button' value="Receive">
							</p>
						</div>
						
					</div>
				</form>

			</div>
		</div>

		<!-- Right side -->
		<div class="level-right">
			<div class="level-item">
				<a class="button is-success" id="complete-transaction-button">Complete</a>
			</div>
			<div class="level-item">
				<a class="button is-success" id="add-product-button">Add New Product</a>
			</div>
		</div>
	</nav>

	<div class="columns">
		<div class="column is-12">

			<table class="table" id="receive-items">
				<thead>
					<tr>
						<th>Barcode</th>
						<th>Product Name</th>
						<th>Quantity Rec'd</th>
						<th>Total Store Quantity</th>
						<th>New Store Quantity</th>
					</tr>
				</thead>
				<tbody>
					
				</tbody>
			</table>
		</div>
		
	</div>

</section>

<input type="hidden" id="_token" value="{{!! csrf_token() !!}}">
<input type="hidden" id="storeID" value="{{$store->id}}">


@endsection

@section('js')
	<script src="/js/receive.js"></script>
@endsection