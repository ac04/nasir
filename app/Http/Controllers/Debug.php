<?php

namespace App\Http\Controllers;

class Debug {
	

	public static function dump() {


		echo "<pre>";

		$arg_list = func_get_args();
	    foreach ($arg_list as $arg) {
	        var_dump($arg);
	    }

	}

	public static function dumpAndDie() {
		self::dump(func_get_args());
		exit;
	}

}