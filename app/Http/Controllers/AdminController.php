<?php

	namespace App\Http\Controllers;
	use Illuminate\Http\Request;
	use Illuminate\Support\Facades\Crypt;
	use App\User;
	use Illuminate\Support\Facades\Auth;
	use App\Http\Models\Branch;
	use App\Http\Models\Transaction;
	use App\Http\Models\Product;
	use Illuminate\Support\Facades\DB;

	/**
	* 
	*/
	class AdminController extends Controller
	{
		
		function __construct()
		{
			# code...
		}



		public function doLogin(Request $request) {

			// TODO implement properly

			return redirect()->route('StockHome');	
			


		}

		public function homeAction(Request $request) {

			if (!Auth::check()) {
				return redirect()->route('EposLogin');
			}

			if (!Auth::user()->is_admin) {
				return redirect()->route('MainMenu');
			}

			$report_type = 'stock';

			$branches = Branch::all();

			return view('admin.main', ['branches' => $branches, 'report_type' => $report_type]);
		}

		public function ordersAction(Request $request) {

			if (!Auth::check()) {
				return redirect()->route('EposLogin');
			}

			if (!Auth::user()->is_admin) {
				return redirect()->route('MainMenu');
			}

			$branches = Branch::get();
			$stock = Branch::find(2)->products()->where('quantity', '<', 2)->orderBy('barcode', 'asc')->get();

			return view('admin.order', [

				'stock' => $stock,
				'branches' => $branches
			]);

		}

		public function viewTransactionsAction(Request $request) {

			if (!Auth::check()) {
				return redirect()->route('EposLogin');
			}

			if (!Auth::user()->is_admin) {
				return redirect()->route('MainMenu');
			}

			$branch = null;
			$transactions = null;
			if ($request->get('branch')) {
				$branch = $request->get('branch');
				$transactions = Transaction::where('branch_id', $request->get('branch'))->orderBy('created_at', 'desc')->paginate(10);

			} else {
				$transactions = Transaction::orderBy('created_at', 'desc')->paginate(10);

			}
			$branches = Branch::all();
			return view('admin.transactionlist', ['transactions' => $transactions, 'branch' => $branch, 'branches' => $branches]);

		}

		public function viewProductsAction(Request $request) {

			if (!Auth::check()) {
				return redirect()->route('EposLogin');
			}

			if (!Auth::user()->is_admin) {
				return redirect()->route('MainMenu');
			}

			$products = Product::orderBy('barcode', 'asc')->paginate(10);

			return view('admin.productlist', ['products' => $products]);

		}

		public function viewSingleProductAction(Request $request, $id) {

			if (!Auth::check()) {
				return redirect()->route('EposLogin');
			}

			if (!Auth::user()->is_admin) {
				return redirect()->route('MainMenu');
			}

			$product = Product::where('id','=',$id)->get()->first();

			return view('admin.productview', ['product' => $product]);

		}

		public function manageProductStockAction(Request $request, $id) {

			if (!Auth::check()) {
				return redirect()->route('EposLogin');
			}

			if (!Auth::user()->is_admin) {
				return redirect()->route('MainMenu');
			}

			$product = Product::where('id','=',$id)->get()->first();
			$branches = Branch::get()->toArray();
			$hours24 = 0; // 24*60*60 = 86400 seconds
			$days7 = 0; // 86400 * 7 = 604800 seconds
			$days30 = 0; // 86400 * 30 = 2592000 seconds
			$recentChanges = [];
			$branchesSorted = [];

			foreach($branches as $branch) {
				$recentChanges[$branch['id']] = new \StdClass();
				$recentChanges[$branch['id']]->lastOrderDate = "No order information";
				$recentChanges[$branch['id']]->lastOrderQuantity = 0;
				$recentChanges[$branch['id']]->sales24Hours = 0;
				$recentChanges[$branch['id']]->sales7Days = 0;
				$recentChanges[$branch['id']]->sales30Days = 0;

				$branchesSorted[$branch['id']] = $branch;
			}

			$results = DB::table('transaction_products')->where('product_id',$id)->orderBy('transaction_id', 'asc')->get();
			foreach($results as $result) {
				
				$transaction = Transaction::find($result->transaction_id);


				if ($transaction->type == "sale") {
					if (time() - strtotime($transaction->created_at) < 86400) {

						$recentChanges[$transaction->branch_id]->sales24Hours += $result->quantity;

					}
					if (time() - strtotime($transaction->created_at) < 604800) {
						$recentChanges[$transaction->branch_id]->sales7Days += $result->quantity;
					}
					if (time() - strtotime($transaction->created_at) < 2592000) {
						$recentChanges[$transaction->branch_id]->sales30Days += $result->quantity;
					}
				} elseif ($transaction->type == "receive") {
					$recentChanges[$transaction->branch_id]->lastOrderDate = date("Y-m-d H:i:s", strtotime($transaction->created_at));
					$recentChanges[$transaction->branch_id]->lastOrderQuantity = $result->quantity;
				}

			}

			// Debug::dumpAndDie($recentChanges);
			

			return view('admin.productviewstock', ['product' => $product, 'branches' => $branchesSorted, 'recentChanges' => $recentChanges]);

		}

		public function viewSingleTransactionAction(Request $request, $id) {

			if (!Auth::check()) {
				return redirect()->route('EposLogin');
			}

			if (!Auth::user()->is_admin) {
				return redirect()->route('MainMenu');
			}

			$transaction = Transaction::find($id);

			return view('admin.transactionview', ['transaction' => $transaction]);

		}

		public function doTransferStock(Request $request, $id) {

			if (!Auth::check()) {
				return redirect()->route('EposLogin');
			}

			if (!Auth::user()->is_admin) {
				return redirect()->route('MainMenu');
			}

			$stockSorted = [];
			$productStock = Product::find($id)->stockLevels()->get()->toArray();

			foreach($productStock as $branch) {
				$stockSorted[$branch['id']] = $branch['pivot'];
			}

			// record transaction from branch

			$transaction = new Transaction();
			$transaction->branch_id = $request->input('branch_from');
			$transaction->user_id = Auth::user()->id;
			$transaction->subtotal = 0;
			$transaction->vat = 0;
			$transaction->total = 0;
			$transaction->payment_type = null;
			$transaction->type = "transfer from";
			$transaction->save();
			
			$transaction->products()->attach($id, ['quantity' => 0-$request->input('quantity')]);
			
			// Debug::dumpAndDie($request->input());
			$newStockQuantity = $stockSorted[$request->input('branch_from')]['quantity'] - $request->input('quantity');

			DB::table('branch_products')
			->where('branch_id', $request->input('branch_from'))
			->where('product_id', $id)
			->update(['quantity' => $newStockQuantity, 'updated_at' => date("Y-m-d H:i:s")]);

			// record transaction to branch

			$transaction = new Transaction();
			$transaction->branch_id = $request->input('branch_to');
			$transaction->user_id = Auth::user()->id;
			$transaction->subtotal = 0;
			$transaction->vat = 0;
			$transaction->total = 0;
			$transaction->payment_type = null;
			$transaction->type = "transfer to";
			$transaction->save();

			$transaction->products()->attach($id, ['quantity' => $request->input('quantity')]);
			
			$newStockQuantity = $stockSorted[$request->input('branch_to')]['quantity'] + $request->input('quantity');		
	
			DB::table('branch_products')
			->where('branch_id', $request->input('branch_to'))
			->where('product_id', $id)
			->update(['quantity' => $newStockQuantity, 'updated_at' => date("Y-m-d H:i:s")]);


			return redirect()->to("/admin/product/view/{$id}/stock");

		}


		/*



		Route::get('/admin/transaction/view/{id}', 'StockControlController@viewSingleTransactionAction');
Route::get('/admin/products', 'StockControlController@viewTransactionsAction');
Route::get('/admin/product/view/{id}', 'StockControlController@viewSingleProductAction');
*/
	}

