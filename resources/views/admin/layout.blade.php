<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>@yield('title')</title>
		<link rel="stylesheet" href="/css/bulma.min.css">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">
		<link rel="stylesheet" href="/css/app.css">
	</head>
	<body class="admin @yield('bodyClass')">

		@include('main.navbar')
		<div class="columns">
			<aside class="column is-3 aside hero is-fullheight is-hidden-mobile">
				
				@yield('left-navbar')
			</aside>
			<div class="content column is-9 scrollable">
				@yield('content')
			</div>
		</div>
		<footer class="footer">
			<div class="container">
				<div class="has-text-centered">
					<p>
						<strong>Bulma</strong> by <a href="http://jgthms.com">Jeremy Thomas</a>. The source code is licensed
						<a href="http://opensource.org/licenses/mit-license.php">MIT</a>. The website content
						is licensed <a href="http://creativecommons.org/licenses/by-nc-sa/4.0/">CC ANS 4.0</a>.
					</p>
					<p>
						<a class="icon" href="https://github.com/jgthms/bulma">
							<i class="fa fa-github"></i>
						</a>
					</p>
				</div>
			</div>
		</footer>
		<script src="/js/jquery.min.js"></script>
		<script src="/js/underscore.min.js"></script>
		<script src="/js/admin.js"></script>
	</body>
</html>