@extends('admin.layout')

@section('content')


<table id="transaction-list">
	<thead>
		<tr>
			<th>Date</th>
			<th>Type</th>
			<th>User</th>
			<th>Branch</th>
			<th class="has-text-right">Total</th>

		</tr>
	</thead>
	<tbody>
		@foreach($transactions as $transaction)
		<tr data-transaction-id="{{$transaction->id}}">
			<td>{{$transaction->created_at}}</td>
			<td>{{$transaction->type}}</td>
			<td>{{$transaction->user->user_id}}</td>
			<td>{{$transaction->branch->name}}</td>
			<td class="has-text-right">£{{$transaction->total}}</td>
		</tr>

		@endforeach
	</tbody>
</table>

@if ($branch != null)
	{{ $transactions->appends(['branch' => $branch])->links('vendor.pagination.default')}}
@else 
{{ $transactions->links('vendor.pagination.default')}}

@endif

@endsection

@section('left-navbar')

<div>
					
	<div class="main">
		<div class="title">Select Branch</div>

		<a href="/admin/transactions" class="item active">
				<span class="icon">
					<i class="fa fa-home"></i>
				</span>
				<span class="name">All Branches</span>
			</a>

		@foreach($branches as $branch)
			
			<a href="/admin/transactions/?branch={{$branch->id}}" class="item active" title="{{$branch->name}}">
				<span class="icon">
					<i class="fa fa-home"></i>
				</span>
				<span class="name">{{$branch->getNameTruncated(19)}}</span>
			</a>
		@endforeach

	</div>
</div>

@endsection