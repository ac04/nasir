<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>@yield('title')</title>
		<link rel="stylesheet" href="/css/bulma.min.css">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">
		<link rel="stylesheet" href="/css/app.css">
	</head>
	<body class="@yield('bodyClass')">
		@yield('nav')
		@yield('content')
		<script src="/js/jquery.min.js"></script>
		<script src="/js/underscore.min.js"></script>
		@section('js')
			 <script src="/js/app.js"></script>
		@show
    </body>
</html>