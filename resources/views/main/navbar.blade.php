<nav class="navbar main-nav" role="navigation" aria-label="main navigation">
	<div class="navbar-brand">
		<a class="navbar-item" href="/">
			<img src="http://placehold.it/100x100" alt="Bulma: a modern CSS framework based on Flexbox" > Home
		</a>
	</div>
	<div class="navbar-start">
		<a class="navbar-item" href='/stock/sell'>Sell Stock</a>
		<a class="navbar-item" href='/stock/receive'>Receive Stock</a>
		@if (Auth::user()->is_admin)
		<div class="navbar-item has-dropdown is-hoverable">
			<a class="navbar-link " href="/admin/">
				Admin
			</a>
			<div class="navbar-dropdown ">
				<a class="navbar-item" href="/admin/products">
					Manage Stock
				</a>
				<a class="navbar-item" href="/admin/transactions">
					View Transactions
				</a>
				<!-- <a class="navbar-item" href="">
					Layout
				</a>
				<a class="navbar-item" href="">
					Form
				</a>
				<a class="navbar-item" href="">
					Elements
				</a>
				
				<a class="navbar-item" href="">
					Components
				</a> -->
				
			</div>
		</div>
		@endif 
	</div>
	<div class="navbar-end">
		<a class="navbar-item" href="/auth/logout">Logout</a>
	</div>
		<button class="button navbar-burger">
			<span></span>
			<span></span>
			<span></span>
		</button>
	</div>	
</nav>