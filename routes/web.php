<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'MainController@menuAction')->name('MainMenu');

Route::get('/stock/sell', 'EposController@sellAction')->name('EposSell');

Route::get('/stock/receive', 'EposController@receiveAction')->name('EposReceive');

Route::get('/stock/receive/summary/{id}', 'EposController@receiptSummaryAction');

Route::get('/auth/login', 'MainController@loginAction')->name('EposLogin');

Route::post('/auth/login', 'MainController@doLogin');

Route::get('/auth/logout', 'MainController@doLogout');

Route::get('/admin', 'AdminController@homeAction')->name('StockHome');

Route::get('/admin/orders', 'AdminController@ordersAction');

Route::get('/admin/transactions', 'AdminController@viewTransactionsAction');
Route::get('/admin/transaction/view/{id}', 'AdminController@viewSingleTransactionAction');
Route::get('/admin/products', 'AdminController@viewProductsAction');
Route::get('/admin/product/view/{id}', 'AdminController@viewSingleProductAction');
Route::get('/admin/product/view/{id}/stock', 'AdminController@manageProductStockAction');
Route::post('/admin/product/view/{id}/stock/transfer', 'AdminController@doTransferStock');
Route::get('/admin/product/view/{id}/update', 'AdminController@doUpdateProduct');

Route::get('/import/products', 'ImportController@import');

Route::get('/import/createbranches', 'ImportController@createBranches');

Route::get('/api/product', 'ApiController@getProduct');
Route::get('/api/productstock', 'ApiController@getProductWithStockLevels');



Route::post('/api/transaction', 'ApiController@recordTransaction');

// Route::get('/store/select', 'MainController@changeStoreAction');

Route::get('/store/select/{id}', 'MainController@doChangeStore');

Route::post('api/product/add', 'ApiController@addProduct');
Route::post('api/addstock', 'ApiController@addStock');

Route::get('/token/get', 'ApiController@getToken');