@extends('pos.layout')

@section('title', 'Login | EPOS | Stock Management')

@section('bodyClass', 'pos-changestore')

@section('content')

@foreach(Auth::user()->branches as $store)

	<a href='/change/store/{{$store->id}}'>{{$store->name}}</a>

@endforeach


@endsection