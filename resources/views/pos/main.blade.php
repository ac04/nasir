@extends('pos.layout')
@section('bodyClass', 'pos-main')
@section('content')
<input type="hidden" id="user_id" value="{{$user_id}}">
<input type="hidden" id="branch_id" value="{{$branch_id}}">
<input type="hidden" id="csrf_token" value="{{ csrf_token() }}">

<section class="section">
	<div class="columns">
		<div class="column column1 is-6">
			<div class="container is-fluid">
				<form action="" class="barcode-quantity">
					<div class="field">
						<p class="control">
							<div class="columns">
								<div class="column is-2">
									<strong class='barcode-quantity-label'>Barcode: </strong>
								</div>
								<div class="column is-10">
									<input class="input" type="text" placeholder="Enter a barcode" id="barcode">
								</div>
							</div>
							
						</p>
					</div>
				</form>
				<div class="transactions-list">
					<div class="total">
						<table class="table">
							<tr>
								<td class="has-text-left">Total:</td>
								<td class="has-text-right" id="total">£0.00</td>
								
							</tr>
						</table>
					</div>
					<table class="table transactions">
						<tr class='unselectable'>
							<th>Qty/kg</th>
							<th>Description</th>
							<th>Price</th>
							<th>Total</th>
						</tr>
						<tr class='unselectable'>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
						</tr>
						
					</table>
					
				</div>
				
				<div class="columns is-multiline ">
					<div class="column is-3">
						<a href="/auth/logout" class="button is-large is-danger is-outlined">Logout</a>
					</div>
					<div class="column is-3">
						<a href="/" class="button is-large is-danger is-outlined">Menu</a>
					</div>
					<div class="column is-3">
						<a href="" class="button is-large is-info is-outlined void-item-button" >Void Item</a>
					</div>
					<div class="column is-3">
						<a href="" class="button is-large is-info is-outlined void-basket-button" >Void Basket</a>
					</div>
				</div>
			</div>
		</div>
		<div class="column column3 is-6">
			<div class="keypad">
				<div class="columns is-multiline ">
					<div class="column is-12 top-keys">
						<div class="columns is-multiline">
							<div class="column is-9">
								<div class="columns is-multiline">
									<div class="column is-4">
										<a href="" class="button is-large is-info is-outlined quantity-button">Qty</a>
									</div>
									<div class="column is-4">
										
									</div>
									<div class="column is-4">
										
									</div>
								</div>
							</div>
							
						</div>
					</div>
					<div class="column is-12 bottom-keys">
						<div class="columns is-multiline">
							<div class="column is-9">
								<div class="columns is-multiline">
									<div class="column is-4">
										<a href="" class="button is-large is-info is-outlined number-button" data-num-val="7">7</a>
									</div>
									<div class="column is-4">
										<a href="" class="button is-large is-info is-outlined number-button" data-num-val="8">8</a>
									</div>
									<div class="column is-4">
										<a href="" class="button is-large is-info is-outlined number-button" data-num-val="9">9</a>
									</div>
									<div class="column is-4">
										<a href="" class="button is-large is-info is-outlined number-button" data-num-val="4">4</a>
									</div>
									<div class="column is-4">
										<a href="" class="button is-large is-info is-outlined number-button" data-num-val="5">5</a>
									</div>
									<div class="column is-4">
										<a href="" class="button is-large is-info is-outlined number-button" data-num-val="6">6</a>
									</div>
								</div>
								
							</div>
							<div class="column is-3">
								<a href="" class="button is-large is-warning enter-button">Enter</a>
							</div>
							<div class="column is-9">
								<div class="columns is-multiline ">
									<div class="column is-4">
										<a href="" class="button is-large is-info is-outlined number-button" data-num-val="1">1</a>
									</div>
									<div class="column is-4">
										<a href="" class="button is-large is-info is-outlined number-button" data-num-val="2">2</a>
									</div>
									<div class="column is-4">
										<a href="" class="button is-large is-info is-outlined number-button" data-num-val="3">3</a>
									</div>
									<div class="column is-4">
										<a href="" class="button is-large is-info is-outlined number-button" data-num-val="0">0</a>
									</div>
									<div class="column is-4">
										<a href="" class="button is-large is-info is-outlined number-button" data-num-val="00">00</a>
									</div>
									<div class="column is-4">
										<a href="" class="button is-large is-danger is-outlined delete-button" data-val="">
											<span class="icon is-medium">
												<i class="fa fa-arrow-left"></i>
											</span>
										</a>
									</div>
								</div>
							</div>
							<div class="column is-3">
								<a href="" class="button is-large is-success checkout-button" disabled>Checkout</a>
							</div>
						</div>
					</div>
					
					
					
					
				</div>
			</div>
		</div>
	</div>
</section>
@endsection