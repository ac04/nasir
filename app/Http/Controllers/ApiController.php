<?php

	namespace App\Http\Controllers;
	use Illuminate\Http\Request;
	use App\Http\Models\Product;
	use App\Http\Models\Transaction;
	use App\Http\Models\Branch;
	use App\Http\Responses\JSONResponse;
	use App\Http\Responses\ProductStockQuantityJSONResponse;
	use App\Http\Responses\ProductJSONResponse;
	use Illuminate\Support\Facades\Auth;
	use Illuminate\Support\Facades\DB;

	class ApiController extends Controller {

		// return a json response with just product details for the requested product (GET request - ID)

		public function getProduct(Request $request) {
			
			$response = new ProductJSONResponse();

			$barcode = $request->input('barcode');

			$product = Product::where('barcode', '=', $barcode)->first();
			if (count($product) > 0) {
				$response->status = "success";
				$response->product = $product;
			} else {
				$response->status = "error";
			}

			return json_encode($response);


		}

		// return a json response with the product details and branch stock levels

		public function getProductWithStockLevels(Request $request) {

			$response = new ProductStockQuantityJSONResponse();

			$barcode = $request->input('barcode');

			$product = Product::where('barcode', '=', $barcode)->first();

			$stock = $product->stockLevels()->get()->toArray();
			
			foreach($stock as $branch) {
				$response->stockQuantities[$branch['id']] = $branch['pivot'];
			}
			if (count($product) > 0) {
				$response->status = "success";
				$response->product = $product;
			} else {
				$response->status = "error";
			}

			return json_encode($response);

		}


		// add a new product to the database that does not exist yet

		public function addProduct(Request $request) {

			$response = new JSONResponse();

			$product = new Product();
			$product->barcode = $request->input('productBarcode');
			$product->description = $request->input('productName');
			$product->cost_price = $request->input('productPrice');
			$product->vat = $request->input('productVat');
			$product->markup = $request->input('productMarkup');
			$product->retail_price = $request->input('productRetailPrice');
			$product->department = $request->input('productDepartment');
			$product->save();

			$branches = Branch::all();

			foreach($branches as $branch) {
				
				$branch->products()->attach($product, ['quantity' => 0]);

			}

			if ($product->save()) {
				$response->status = "success";
				$response->data = "Product saved successfully";
			} else {
				$response->status = "error";
				$response->data = "Product unable to save";
			}

			return json_encode($response);

		}

		// record a transaction in the database, add products to the transaction, update branch stock levels for products
		public function recordTransaction(Request $request) {

			$success = false;


			$transaction = new Transaction();
			$transaction->user_id = $request->input('user_id');
			$transaction->branch_id = $request->input('branch_id');
			$transaction->subtotal = $request->input('subtotal');
			$transaction->vat = $request->input('vat');
			$transaction->total = $request->input('total');
			$transaction->payment_type = $request->input('payment_type');
			$transaction->type = $request->input('type');
			

			$response = new JSONResponse();
			$response->data = "";
			if ($transaction->save()) {
				$success = true;
				$response->status = "success";


			} else {

				$response->status = "error";

			}

			$transaction = Transaction::find($transaction->id);

			foreach($request->input('basket') as $basketItem) {
				$product = Product::where('barcode','=', $basketItem['barcode'])->first();
				$transaction->products()->attach($product->id, ['quantity' => $basketItem['quantity']]);

				// update product quantity for branch
				$productStock = Product::find($product->id)->stockLevels()->where('branch_id','=', $request->input('branch_id'))->get()->toArray();

				$newStockQuantity = $productStock[0]['pivot']['quantity'] - $basketItem['quantity'];
		
				
				DB::table('branch_products')
				->where('branch_id', $request->input('branch_id'))
				->where('product_id', $product->id)
				->update(['quantity' => $newStockQuantity, 'updated_at' => date("Y-m-d H:i:s")]);

			}

			return json_encode($response);


		}

		public function addStock(Request $request) {

			$response = new JSONResponse();

			$store = $request->input('store');

			$transaction = new Transaction();
			$transaction->user_id = Auth::user()->id;
			$transaction->branch_id = $store;
			$transaction->subtotal = 0;
			$transaction->vat = 0;
			$transaction->total = 0;
			$transaction->payment_type = null;
			$transaction->type = "receive";
			$transaction->save();


			$stock = $request->input('stock');

			foreach($stock as $stockItem) {

				if ($stockItem != NULL) {

					$productID = $stockItem['product']['id'];
					$addedQuantity = $stockItem['quantity'];

					// add to transaction with how many added

					$transaction->products()->attach($productID, ['quantity' => $addedQuantity]);

					$productStock = Product::find($productID)->stockLevels()->where('branch_id','=', $store)->get()->toArray();
					
					$newStockQuantity = $productStock[0]['pivot']['quantity'] + $addedQuantity;
			
					
					DB::table('branch_products')
					->where('branch_id', $store)
					->where('product_id', $productID)
					->update(['quantity' => $newStockQuantity, 'updated_at' => date("Y-m-d H:i:s")]);

				}

			}

			$response->status = "success";
			$response->data = $transaction->id;

			return json_encode($response);


		}

		public function getToken(Request $request) {

			return csrf_token();

		}



	}

