<?php

	namespace App\Http\Controllers;
	use Illuminate\Http\Request;
	use Illuminate\Support\Facades\Crypt;
	use App\User;
	use Illuminate\Support\Facades\Auth;
	use App\Http\Models\Branch;
	use App\Http\Models\Transaction;
	use App\Http\Models\Product;

	/**
	* 
	*/
	class EposController extends Controller
	{
		
		function __construct()
		{
			# code...
		}

		

		

		public function sellAction() {

			if (!Auth::check()) {
				return redirect()->route('EposLogin');
			}

			return view('pos.main', ['user_id' => Auth::user()->id, 'branch_id' => session('currentStore')]);
		}

		public function receiveAction() {

			if (!Auth::check()) {
				return redirect()->route('EposLogin');
			}
			return view('pos.receive', ['user_id' => Auth::user()->id, 'branch_id' => session('currentStore'), 'store' => Branch::find(session('currentStore'))]);
		}

		public function receiptSummaryAction(Request $request, $id) {

			if (!Auth::check()) {
				return redirect()->route('EposLogin');
			}

			$transaction = Transaction::where('id','=', $id)->get()->first();
			$transactionProducts = $transaction->products->toArray();

			$transactionProductsSorted = [];
			foreach($transactionProducts as $product) {
				$transactionProductsSorted[$product['barcode']] = $product;
			}
			// Debug::dump($transactionProductsSorted);exit;

			$products = [];
			foreach($transaction->products as $product) {

				$coreProduct = Product::find($product->id);

				// Debug::dumpAndDie($coreProduct);
				$newProduct = [];

				$newProduct['barcode'] = $coreProduct->barcode;
				$newProduct['name'] = $coreProduct->description;

				$newProduct['quantity'] = $transactionProductsSorted[$coreProduct->barcode]['pivot']['quantity'];


				$products[] = $newProduct;

			}

			// Debug::dump(Transaction::where('id','=', $id)->get()->first());

			return view('pos.receiptSummary', ['user_id' => $transaction->user_id, 'branch_id' => $transaction->branch_id, 'store' => Branch::find($transaction->branch_id), 'transaction' => $transaction, 'products' => $products]);

		}

	}