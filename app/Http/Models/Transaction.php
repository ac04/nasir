<?php


	namespace App\Http\Models;

	use Illuminate\Database\Eloquent\Model;

	class Transaction extends Model {

		public function products() {

			return $this->belongsToMany('App\Http\Models\Product', 'transaction_products', 'transaction_id', 'product_id')->withPivot('quantity');

		}

		public function user() {

			return $this->hasOne('App\User', 'id', 'user_id');
		}

		public function branch() {

			return $this->hasOne('App\Http\Models\Branch', 'id', 'branch_id');
		}


	}