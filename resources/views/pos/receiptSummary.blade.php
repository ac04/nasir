@extends('pos.layout')
@section('bodyClass', 'pos-receive')


@section('nav')

	@include('main.navbar')

@endsection


@section('content')




<h1>Stock Received for {{$store->name}} - Transaction {{$transaction->id}} ({{$transaction->created_at}})</h1>

<section>
	

	<table class="table" id="received-items">
		<thead>
			<tr>
				<th>Barcode</th>
				<th>Product Name</th>
				<th>Quantity Rec'd</th>
			</tr>
		</thead>
		<tbody>
			@foreach($products as $product)

				<tr>

					<td>{{$product['barcode']}}</td>
					<td>{{$product['name']}}</td>
					<td>{{$product['quantity']}}</td>		
				</tr>
			@endforeach
		</tbody>
	</table>

</section>
@endsection