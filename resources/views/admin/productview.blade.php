@extends('admin.layout')


@section('content')

<form action="">
							
	<div class="field is-horizontal ">
		<div class="field-label is-normal">
			<label class="label">Name</label>
		</div>
		<div class="field-body">
			<div class="field">
				<div class="control is-expanded">
					<input class="input" id="product-name" type="text" placeholder="Product Name" value="{{$product->description}}">
				</div>
			</div>
		</div>
	</div>
	
	<div class="field is-horizontal">
		<div class="field-label is-normal">
			<label class="label">Barcode</label>
		</div>
		<div class="field-body">
			<div class="field">
				<div class="control is-expanded">
					<input class="input" id="product-barcode" type="text" placeholder="0000111111" value="{{$product->barcode}}">
				</div>
			</div>
		</div>
	</div>

	<div class="field is-horizontal">
		<div class="field-label is-normal">
			<label class="label">Price</label>
		</div>
		<div class="field-body">
			<div class="field has-addons">
				<div class="control">
					<a class="button is-static">£</a>
				</div>
				<div class="control">
					<input class="input" id="product-price" type="text" placeholder="1.50" value="{{$product->cost_price}}">
				</div>
			</div>
		</div>
	</div>

	<div class="field is-horizontal has-addons">
		<div class="field-label is-normal">
			<label class="label">VAT</label>
		</div>
		<div class="field-body">
			<div class="field has-addons">
				<div class="control">
					<input class="input" id="product-vat" type="text" placeholder="20" value="{{$product->vat*100}}">
				</div>
				<div class="control">
					<a class="button is-static">%</a>
				</div>
			</div>
		</div>
	</div>

	<div class="field is-horizontal has-addons">
		<div class="field-label is-normal">
			<label class="label">Markup</label>
		</div>
		<div class="field-body">
			<div class="field has-addons">
				<div class="control">
					<input class="input" id="product-markup" type="text" placeholder="20" value="{{$product->markup}}">
				</div>
				<div class="control">
					<a class="button is-static">%</a>
				</div>
			</div>
		</div>
	</div>

	<div class="field is-horizontal">
		<div class="field-label is-normal">
			<label class="label">Retail Price</label>
		</div>
		<div class="field-body">
			<div class="field has-addons">
				<div class="control">
					<a class="button is-static">£</a>
				</div>
				<div class="control">
					<input class="input" id="product-retail-price" type="text" placeholder="1.50" value="{{$product->retail_price}}">
				</div>
			</div>
		</div>
		
	</div>

	<div class="field is-horizontal">
		<div class="field-label is-normal">
			<label class="label">Department</label>
		</div>
		<div class="field-body">
			<div class="field">
				<div class="control">
					<div class="select">
						<select id="product-department">
							<option value="BABY PRODUCTS" {{$product->department == "BABY PRODUCTS" ? "selected" : ""}}>BABY PRODUCTS</option>
							<option value="BEER & CIDER" {{$product->department == "BEER & CIDER" ? "selected" : ""}}>BEER & CIDER</option>
							<option value="BISCUITS" {{$product->department == "BISCUITS" ? "selected" : ""}}>BISCUITS</option>
							<option value="BREAD & CAKES" {{$product->department == "BREAD & CAKES" ? "selected" : ""}}>BREAD & CAKES</option>
							<option value="CEREALS" {{$product->department == "CEREALS" ? "selected" : ""}}>CEREALS</option>
							<option value="CHILLED" {{$product->department == "CHILLED" ? "selected" : ""}}>CHILLED</option>
							<option value="CIGARETTES" {{$product->department == "CIGARETTES" ? "selected" : ""}}>CIGARETTES</option>
							<option value="CONFECTIONARY" {{$product->department == "CONFECTIONARY" ? "selected" : ""}}>CONFECTIONARY</option>
							<option value="CRISPS & SNACKS" {{$product->department == "CRISPS & SNACKS" ? "selected" : ""}}>CRISPS & SNACKS</option>
							<option value="DRINK/JUICE&MXTR" {{$product->department == "DRINK/JUICE&MXTR" ? "selected" : ""}}>DRINK/JUICE&MXTR</option>
							<option value="FROZEN" {{$product->department == "FROZEN" ? "selected" : ""}}>FROZEN</option>
							<option value="GROCERY" {{$product->department == "GROCERY" ? "selected" : ""}}>GROCERY</option>
							<option value="HEALTH & BEAUTY" {{$product->department == "HEALTH & BEAUTY" ? "selected" : ""}}>HEALTH & BEAUTY</option>
							<option value="HOUSEHOLD" {{$product->department == "HOUSEHOLD" ? "selected" : ""}}>HOUSEHOLD</option>
							<option value="MEDICINE" {{$product->department == "MEDICINE" ? "selected" : ""}}>MEDICINE</option>
							<option value="PET FOODS" {{$product->department == "PET FOODS" ? "selected" : ""}}>PET FOODS</option>
							<option value="SPIRITS" {{$product->department == "SPIRITS" ? "selected" : ""}}>SPIRITS</option>
							<option value="SWEETS" {{$product->department == "SWEETS" ? "selected" : ""}}>SWEETS</option>
							<option value="TEA COFFEE & BVRG" {{$product->department == "TEA COFFEE & BVRG" ? "selected" : ""}}>TEA COFFEE & BVRG</option>
							<option value="WINES" {{$product->department == "WINES" ? "selected" : ""}}>WINES</option>
						</select>
					</div>
				</div>
			</div>
		</div>
	</div>
	

</form>

@endsection

@section('left-navbar')

<div>
	
	<div class="main">

		<a href="/admin/product/view/{{$product->id}}" class="item active">
			<span class="icon">
				<i class="fa fa-home"></i>
			</span>
			<span class="name">Product Details</span>
		</a>

		<a href="/admin/product/view/{{$product->id}}/stock" class="item">
			<span class="icon">
				<i class="fa fa-home"></i>
			</span>
			<span class="name">Manage Stock</span>
		</a>
	
		

	</div>
</div>

@endsection