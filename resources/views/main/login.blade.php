@extends('pos.layout')

@section('title', 'Login | EPOS | Stock Management')

@section('bodyClass', 'pos-login')

@section('content')
	<div class="login-wrapper columns">
		<div class="column is-offset-4 is-4">
			<section class="hero is-fullheight">
				<div class="hero-heading">
					<h1 class="avatar has-text-centered section">
						<img src="/img/logo.jpeg" width="200">
					</h1>
				</div>
				<div class="hero-body">
					<div class="container">
						<div class="columns">
							<div class="column is-8 is-offset-2">
								<form action="/auth/login" method="POST">
									<div class="login-form">

										<p class='error-message has-text-centered'>
											@if (session('error'))
												{{ session('error') }}
											@endif
										</p>
										<p class="control has-icon has-icon-left">
											<input id="user-input" class="input text-input" type="text" name="user_id" placeholder="Username">
											<span class="icon user">
												<i class="fa fa-user"></i>
											</span>
										</p>
										<p class="control has-icon has-icon-left">
											<input id="password-input" class="input password-input" name="pin" type="password" placeholder="●●●●●●">
											<span class="icon user">
												<i class="fa fa-lock"></i>
											</span>
										</p>
										<p class="control login">
											<button class="button is-success is-outlined is-large is-fullwidth">Login</button>
										</p>
									</div>
									<input type="hidden" name="_token" value="{{ csrf_token() }}">
								</form>
							</div>
						</div>
					</div>
				</div>
			</section>
		</div>
	</div>
@endsection