<?php


	namespace App\Http\Models;

	use Illuminate\Database\Eloquent\Model;

	class Branch extends Model {

		public function products() {

			return $this->belongsToMany('App\Http\Models\Product', 'branch_products', 'branch_id', 'product_id')->withPivot('quantity')->withTimestamps();

		}

		public function getNameTruncated($length = 10) {
			if (strlen($this->name) > $length) {
				return substr($this->name, 0, $length) . "&hellip;";
			} 
			return $this->name;
		}


		public function users() {
			return $this->belongsToMany('App\User', 'branch_users', 'user_id', 'branch_id');
		}


	}


	