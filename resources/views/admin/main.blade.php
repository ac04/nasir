@extends('admin.layout')

@section('left-navbar')

<div>
					
	<div class="main">
		<div class="title">Select Branch</div>

		<a href="/admin/report/{{$report_type}}/all" class="item active">
				<span class="icon">
					<i class="fa fa-home"></i>
				</span>
				<span class="name">All Branches</span>
			</a>

		@foreach($branches as $branch)
			
			<a href="/admin/report/{{$report_type}}/{{$branch->id}}" class="item active" title="{{$branch->name}}">
				<span class="icon">
					<i class="fa fa-home"></i>
				</span>
				<span class="name">{{$branch->getNameTruncated(19)}}</span>
			</a>
		@endforeach

	</div>
</div>
@endsection