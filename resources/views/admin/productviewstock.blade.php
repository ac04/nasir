@extends('admin.layout')


@section('content')


<table id="stock-list">
	<thead>
		<tr>
			<th>Branch</th>
			<th>Stock</th>
			<th>Sold 24 hours</th>
			<th>Sold 7 days</th>
			<th>Sold 30 days</th>
			<th>Last Delivery</th>
		</tr>
	</thead>
	<tbody>
		

		@foreach($product->stockLevels as $stockLevel)
		<tr>
			<td>{{$branches[$stockLevel->pivot->branch_id]['name']}}</td>
			<td>{{$stockLevel->pivot->quantity}}</td>
			<td>{{$recentChanges[$stockLevel->pivot->branch_id]->sales24Hours}}</td>
			<td>{{$recentChanges[$stockLevel->pivot->branch_id]->sales7Days}}</td>
			<td>{{$recentChanges[$stockLevel->pivot->branch_id]->sales30Days}}</td>
			<td>{{$recentChanges[$stockLevel->pivot->branch_id]->lastOrderDate}} ({{$recentChanges[$stockLevel->pivot->branch_id]->lastOrderQuantity}})</td>
		</tr>

		@endforeach
	</tbody>
</table>

<h2>Transfer Stock</h2>
<form action="/admin/product/view/{{$product->id}}/stock/transfer" method="POST">
	<div class="column is-4">
		<div class="field">
			<label class="label">Quantity</label>
			<div class="control">
				<input class="input" name="quantity" type="text" placeholder="Quantity">
			</div>
		</div>

		<div class="field">
			<label class="label">From Store</label>
			<div class="control">
				<div class="select">
					<select name="branch_from">
						@foreach($branches as $branch)
							<option value="{{$branch['id']}}">{{$branch['name']}}</option>
						@endforeach

					</select>
				</div>
			</div>
		</div>

		<div class="field">
			<label class="label">To Store</label>
			<div class="control">
				<div class="select">
					<select name="branch_to">
						@foreach($branches as $branch)
							<option value="{{$branch['id']}}">{{$branch['name']}}</option>
						@endforeach

					</select>
				</div>
			</div>
		</div>
		<input class="button is-primary" type="submit" value="Transfer">
	</div>
	<input type="hidden" name="_token" value="{{csrf_token()}}">


</form>



@endsection

@section('left-navbar')

<div>
	
	<div class="main">

		<a href="/admin/product/view/{{$product->id}}" class="item ">
			<span class="icon">
				<i class="fa fa-home"></i>
			</span>
			<span class="name">Product Details</span>
		</a>

		<a href="/admin/product/view/{{$product->id}}/stock" class="item active">
			<span class="icon">
				<i class="fa fa-home"></i>
			</span>
			<span class="name">Manage Stock</span>
		</a>
	
		

	</div>
</div>

@endsection