@extends('pos.layout')

@section('title', 'Menu | EPOS | Stock Management')

@section('bodyClass', 'main-menu')

@section('content')
	<div class="main-wrapper columns">
		<div class="container">
			<div class="column is-offset-2 is-8">
				<div class="content">
					<h1 class="has-text-centered">Welcome {{Auth::user()->name}}</h1>
					<h2 class='has-text-centered'>Store: {{$store ? $store->name : "User not assigned to any stores" }}</h2>
				</div>

				<div class="columns is-multiline">
					<div class="column is-4 is-offset-2 has-text-centered">
						<a href="/stock/sell" class="button is-info is-large is-outlined " {{ $store ? '' : 'disabled' }}>Sell Stock</a>
					</div>
					<div class="column is-4 has-text-centered">
						<a href="/stock/receive" class="button is-info is-large is-outlined" {{ $store ? '' : 'disabled' }}>Receive Stock</a>
					</div>

					@if (Auth::user()->is_admin)
					<div class="column is-4 is-offset-4 has-text-centered">
						<a href="/admin" class="button is-danger is-large is-outlined">Admin</a>
					</div>
					@endif
					
					@if (Auth::user()->hasMultipleStores())
					<div class="column is-4 is-offset-4">
						<div class="dropdown store-select">
							<div class="dropdown-trigger">
								<button class="button" aria-haspopup="true" aria-controls="dropdown-menu">
								<span>Change Store</span>
								<span class="icon is-small">
								<i class="fa fa-angle-down" aria-hidden="true"></i>
								</span>
								</button>
							</div>
							<div class="dropdown-menu" id="dropdown-menu" role="menu">
								<div class="dropdown-content">
									@if (Auth::user()->branches)
										@foreach(Auth::user()->branches as $branch)
											<a href="/store/select/{{$branch->id}}" class="dropdown-item">{{$branch->name}}</a>
										@endforeach
									@endif
									<!-- <a class="dropdown-item">
									Other dropdown item
									</a>
									<a href="#" class="dropdown-item is-active">
									Active dropdown item
									</a>
									<a href="#" class="dropdown-item">
									Other dropdown item
									</a>
									<hr class="dropdown-divider">
									<a href="#" class="dropdown-item">
									With a divider
									</a> -->
								</div>
							</div>
						</div>
					</div>
					

					@endif
					
					<div class="column is-4 is-offset-4 has-text-centered">
						<a href="/auth/logout" class="button is-primary is-large is-outlined">Logout</a>
					</div>


				</div>
			</div>
		
		</div>
	</div>
@endsection