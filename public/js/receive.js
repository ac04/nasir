var stock = {};
var sortOrder = [];


(function() {

	
	$('#add-product-button').on('click', function() {
		createAddProductWindow();
	});

	$('body').on('click', '#add-product-modal .delete', function() {
		$('#add-product-modal').remove();
	});

	$('body').on('click', '#cancel-add-product', function() {
		$('#add-product-modal').remove();
	});

	$('body').on('click', '#confirm-add-product', function() {
		addProductToDatabase();
	});

	$('#receive-stock-form').on('submit', function() {
		

		var barcode = $('#barcode').val();
		var quantity = $('#quantity').val();

		if (barcode == "") {
			return false;
		}
		if (quantity == "") {
			$('#quantity').addClass('is-danger');	
			return false;
		} else {
			$('#quantity').removeClass('is-danger');
		}
		$.get('/api/productstock', {barcode: $('#barcode').val()}, function(response) {
			
			var response = JSON.parse(response);
			console.log(response);
			if (response.status == "error") {
				return false;
			}

			var product = response.product;
			var stockQuantity = response.stockQuantities;

			stock[product.id] = {
				product: product,
				quantity: quantity,
				stockQuantity: stockQuantity
			};
			sortOrder.push(product.id);
			renderTable();

		});

		$(this)[0].reset();
		return false;
	});

	$('body').on('click', '#complete-transaction-button', function() {

		var store = $('#storeID').val();
		//$.post('') // record transaction once with 0 values and type = receipt

		// $.post // add stock quantity to database
		var token = null;
		$.get('/token/get', function(newToken) {

			token = newToken;

			$.post('/api/addstock', {_token: token, stock: stock, store: store }, function(response) {
				response = JSON.parse(response);
				if (response.status == "success") {
					window.location = "/stock/receive/summary/"+response.data;
				}

			
			});
		})
		
		return false;

	});

	function renderTable() {
		$("#receive-items tbody").html("");

		console.log('rendering table');
		var storeID = Number($('#storeID').val());
		sortOrder.forEach(function(element) {
			console.log(stock[element]);

			var html = `
				<tr>
					<td>${stock[element].product.barcode}</td>
					<td>${stock[element].product.description}</td>
					<td>${stock[element].quantity}</td>
					<td>${stock[element].stockQuantity[storeID].quantity}</td>
					<td>${Number(stock[element].quantity) + Number(stock[element].stockQuantity[storeID].quantity)}</td>
				</tr>
			`;
			$("#receive-items tbody").append(html);
		});
	}

	function createAddProductWindow() {
		var html = `
			<div class="modal" id="add-product-modal">
				<div class="modal-background"></div>
				<div class="modal-card">
				<header class="modal-card-head">
					<p class="modal-card-title">Add New Product</p>
					<button class="delete" aria-label="close"></button>
				</header>
				<section class="modal-card-body">
					<form action="">
							
						<div class="field is-horizontal ">
							<div class="field-label is-normal">
								<label class="label">Name</label>
							</div>
							<div class="field-body">
								<div class="field">
									<div class="control is-expanded">
										<input class="input" id="product-name" type="text" placeholder="Product Name">
									</div>
								</div>
							</div>
						</div>
						
						<div class="field is-horizontal">
							<div class="field-label is-normal">
								<label class="label">Barcode</label>
							</div>
							<div class="field-body">
								<div class="field">
									<div class="control is-expanded">
										<input class="input" id="product-barcode" type="text" placeholder="0000111111">
									</div>
								</div>
							</div>
						</div>

						<div class="field is-horizontal">
							<div class="field-label is-normal">
								<label class="label">Price</label>
							</div>
							<div class="field-body">
								<div class="field has-addons">
									<div class="control">
										<a class="button is-static">£</a>
									</div>
									<div class="control">
										<input class="input" id="product-price" type="text" placeholder="1.50">
									</div>
								</div>
							</div>
						</div>

						<div class="field is-horizontal has-addons">
							<div class="field-label is-normal">
								<label class="label">VAT</label>
							</div>
							<div class="field-body">
								<div class="field has-addons">
									<div class="control">
										<input class="input" id="product-vat" type="text" placeholder="20">
									</div>
									<div class="control">
										<a class="button is-static">%</a>
									</div>
								</div>
							</div>
						</div>

						<div class="field is-horizontal has-addons">
							<div class="field-label is-normal">
								<label class="label">Markup</label>
							</div>
							<div class="field-body">
								<div class="field has-addons">
									<div class="control">
										<input class="input" id="product-markup" type="text" placeholder="20">
									</div>
									<div class="control">
										<a class="button is-static">%</a>
									</div>
								</div>
							</div>
						</div>

						<div class="field is-horizontal">
							<div class="field-label is-normal">
								<label class="label">Retail Price</label>
							</div>
							<div class="field-body">
								<div class="field has-addons">
									<div class="control">
										<a class="button is-static">£</a>
									</div>
									<div class="control">
										<input class="input" id="product-retail-price" type="text" placeholder="1.50">
									</div>
								</div>
							</div>
							
						</div>

						<div class="field is-horizontal">
							<div class="field-label is-normal">
								<label class="label">Department</label>
							</div>
							<div class="field-body">
								<div class="field">
									<div class="control">
										<div class="select">
											<select id="product-department">
												<option value="BABY PRODUCTS">BABY PRODUCTS</option>
												<option value="BEER & CIDER">BEER & CIDER</option>
												<option value="BISCUITS">BISCUITS</option>
												<option value="BREAD & CAKES">BREAD & CAKES</option>
												<option value="CEREALS">CEREALS</option>
												<option value="CHILLED">CHILLED</option>
												<option value="CIGARETTES">CIGARETTES</option>
												<option value="CONFECTIONARY">CONFECTIONARY</option>
												<option value="CRISPS & SNACKS">CRISPS & SNACKS</option>
												<option value="DRINK/JUICE&MXTR">DRINK/JUICE&MXTR</option>
												<option value="FROZEN">FROZEN</option>
												<option value="GROCERY">GROCERY</option>
												<option value="HEALTH & BEAUTY">HEALTH & BEAUTY</option>
												<option value="HOUSEHOLD">HOUSEHOLD</option>
												<option value="MEDICINE">MEDICINE</option>
												<option value="PET FOODS">PET FOODS</option>
												<option value="SPIRITS">SPIRITS</option>
												<option value="SWEETS">SWEETS</option>
												<option value="TEA COFFEE & BVRG">TEA COFFEE & BVRG</option>
												<option value="WINES">WINES</option>
											</select>
										</div>
									</div>
								</div>
							</div>
						</div>
						

					</form>
				</section>
				<footer class="modal-card-foot">
					<button class="button is-success" id="confirm-add-product">Save changes</button>
					<button class="button" id="cancel-add-product">Cancel</button>
				</footer>
				</div>
			</div>
		`;	

		$('.section').append(html);	
		$('#add-product-modal').css({display: "block"});
	}
	


	function addProductToDatabase() {
		
		if ($('#product-name').val() != "" && $('#product-barcode').val() != "" && $('#product-price').val() != "" && $('#product-vat').val() != "" && $('#product-markup').val() != "" && $('#product-retail-price').val() != "" && $('#product-department').val() != "") {

			$('#confirm-add-product').addClass('is-loading').prop('disabled', true);
			
			$.post('/api/product/add', {productName: $('#product-name').val(), productBarcode: $('#product-barcode').val(), productPrice: $('#product-price').val(), productVat: $('#product-vat').val(), productMarkup: $('#product-markup').val(), productRetailPrice: $('#product-retail-price').val(), productDepartment: $('#product-department').val(), _token: $('#_token').val()}, function(response) {
				response = JSON.parse(response);
				if (response.status == "success") {
					$('#add-product-modal').remove();
					// add details to receive form
				}
			});

		} else {
			alert('please fill out the form fully');
		}
		
	}

})();