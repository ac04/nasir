<?php

	namespace App\Http\Controllers;
	use Illuminate\Http\Request;
	use Illuminate\Support\Facades\Crypt;
	use App\User;
	use App\Http\Models\Branch;
	use Illuminate\Support\Facades\Auth;

	/**
	* 
	*/
	class MainController extends Controller
	{
		
		function __construct()
		{
			# code...

			/* Uncomment to create a new user */

			// $user = new User();
			// $user->email = "email@example.com";
			// $user->user_id = "id";
			// $user->password = bcrypt('password');
			// $user->save();
		}

		// display the login screen

		public function loginAction() {



			return view('main.login');

		}

		// display the menu screen

		public function menuAction(Request $request) {
			// echo "<pre>"; 

			// echo "branch: \n";
			// foreach(Auth::user()->branches as $branch) {

			// 	print_r($branch->name);
			// 	echo "\n";


			// }

			// echo "\nusers: \n";
			// foreach(Branch::find(1)->users as $user) {

			// 	print_r($user->name);
			// 	echo "\n";


			// }

			// check if the user is not logged in, if not, redirect to login

			if (!Auth::check()) {

				return redirect()->route('EposLogin');

			} 

			// check if the user has a currentStore stored already, if not, choose the first store they are assigned to
			$store = false;

			if ($request->session()->has('currentStore')) {

				$branches = Auth::user()->branches->toArray();
				
				if ($this->checkBranchInArray($branches, session('currentStore'))) {

					$store = Branch::find(session('currentStore'));

				} else {
					
					if (Auth::user()->branches->count() > 0) {
						
						session(['currentStore' => Auth::user()->branches->first()->id]);
						$store = Branch::find(session('currentStore'));

					}

				}				

			} else {

				// Debug::dump(Auth::user()->branches);
				
				session(['currentStore' => Auth::user()->branches->first()->id]);
				$store = Branch::find(session('currentStore'));

			}

			return view('main.menu', ['store' => $store]);

		}

		// process a login attempt

		public function doLogin(Request $request) {

			
			if (User::where('user_id', '=', $request->input('user_id'))) {
				if (Auth::attempt([
					'user_id' => $request->input('user_id'), 
					'password' => $request->input('pin')]
				)) {

					return redirect()->route('MainMenu');

				} else {
					return redirect()->route('EposLogin')->with('error', 'Invalid Details');

				}
			} else {

				return redirect()->route('EposLogin')->with('error', 'Invalid Details');

			}

			


		}

		// log the user out and redirect to login screen

		public function doLogout(Request $request) {

			Auth::logout();
			$request->session()->flush();

			return redirect()->route('EposLogin');


		}

		// process changing store and return to main menu

		public function doChangeStore(Request $request, $id) {
			
			session(['currentStore' => (int) $id]);

			return redirect()->route('MainMenu');

		}

		private function checkBranchInArray($branches, $id) {

			foreach($branches as $branch) {
				
				if ($branch['id'] == $id) {
					
					return true;

				}

			}
			return false;

		}

	}
