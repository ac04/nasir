@extends('admin.layout')

@section('content')


<table id="product-list">
	<thead>
		<tr>
			<th>Barcode</th>
			<th>Description</th>
			<th>Department</th>
			<th>Date Modified</th>

		</tr>
	</thead>
	<tbody>
		@foreach($products as $product)
		<tr data-product-id="{{$product->id}}">
			
			<td>{{$product->barcode}}</td>
			<td>{{$product->description}}</td>
			<td>{{$product->department}}</td>
			<td>{{$product->updated_at}}</td>
		</tr>

		@endforeach
	</tbody>
</table>


{{ $products->links('vendor.pagination.default')}}

@endsection

@section('left-navbar')



@endsection