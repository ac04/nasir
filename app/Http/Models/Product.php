<?php


	namespace App\Http\Models;

	use Illuminate\Database\Eloquent\Model;

	class Product extends Model {

		
		public function transactions() {

			return $this->belongsToMany('App\Http\Models\Product', 'transaction_products', 'product_id', 'id');
		}

		public function stockLevels() {

			return $this->belongsToMany('App\Http\Models\Branch', 'branch_products', 'product_id', 'branch_id')->withPivot('quantity')->withTimestamps();

		}

	}